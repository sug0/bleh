/* bleh -- feh-like image viewer for Mac
   a fork of meh [https://github.com/takeiteasy/meh]

 The MIT License (MIT)

 Copyright (c) 2024 Tiago Carvalho
 Copyright (c) 2022 George Watson

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#import "cocoa_keys.h"
#import <Cocoa/Cocoa.h>

#ifdef DEBUG
#define LOG(...) NSLog(@"DEBUG: " __VA_ARGS__)
#else
#define LOG(...)
#endif

#define PANIC(...)                                                             \
    do {                                                                       \
        alert(NSAlertStyleCritical, @"ERROR! " __VA_ARGS__);                   \
        [NSApp terminate:nil];                                                 \
    } while (0)

#define WARN(...) alert(NSAlertStyleWarning, @"WARNING! " __VA_ARGS__)

const BOOL enableResizeAnimations = YES;

static BOOL alert(enum NSAlertStyle style, NSString *fmt, ...) {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setAlertStyle:style];
    [alert addButtonWithTitle:@"OK"];
    va_list args;
    va_start(args, fmt);
    NSString *msg = [[NSString alloc] initWithFormat:fmt arguments:args];
    va_end(args);
    [alert setMessageText:msg];
    return [alert runModal] == NSAlertFirstButtonReturn;
}

static BOOL checkPrefix(NSData *inputData, size_t mustLen, const char *prefix,
                        size_t prefixLen) {
    return [inputData length] >= mustLen &&
           !memcmp([inputData bytes], prefix, prefixLen);
}

static CGFloat floatMin(CGFloat a, CGFloat b) { return (a < b) ? a : b; }

static BOOL isFarbfeldImage(NSData *inputData) {
    return checkPrefix(inputData, 16, "farbfeld", 8);
}

static BOOL isDecodableByCocoa(NSData *inputData) {
#define _PNG_MAGIC ("\x89PNG\r\n\x1a\n")
#define _PNG_MAGIC_LEN (sizeof(_PNG_MAGIC) - 1)
#define _JPG_MAGIC ("\xff\xd8\xff")
#define _JPG_MAGIC_LEN (sizeof(_JPG_MAGIC) - 1)

    return checkPrefix(inputData, _PNG_MAGIC_LEN, _PNG_MAGIC, _PNG_MAGIC_LEN) ||
           checkPrefix(inputData, _JPG_MAGIC_LEN, _JPG_MAGIC, _JPG_MAGIC_LEN);

#undef _PNG_MAGIC
#undef _PNG_MAGIC_LEN
#undef _JPG_MAGIC
#undef _JPG_MAGIC_LEN
}

static NSImage *decodeFarbfeldImage(NSData *inputData) {
    const size_t width = (size_t)ntohl(*(uint32_t *)([inputData bytes] + 8));
    const size_t height = (size_t)ntohl(*(uint32_t *)([inputData bytes] + 12));

    const size_t bufferLength = [inputData length] - 16;
    const uint8_t *bitmap = [inputData bytes] + 16;

    if ((8 * width * height) != bufferLength) {
        WARN("Invalid Farbfeld image detected");
        return NULL;
    }

    CGDataProviderRef dataProvider =
        CGDataProviderCreateWithData(NULL, bitmap, bufferLength, NULL);

    const size_t bitsPerComponent = 16;
    const size_t bitsPerPixel = 64;
    const size_t bytesPerRow = 8 * width;

    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo =
        kCGBitmapByteOrder16Big | kCGImageAlphaPremultipliedLast;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;

    CGImageRef iref =
        CGImageCreate(width, height, bitsPerComponent, bitsPerPixel,
                      bytesPerRow, colorSpaceRef, bitmapInfo, dataProvider,
                      NULL, // null decode array, so colors aren't remapped
                      YES,  // apply interpolation when rescaling the img data
                      renderingIntent);

    NSImage *image =
        [[NSImage alloc] initWithCGImage:iref size:NSMakeSize(width, height)];

    CGDataProviderRelease(dataProvider);
    CGImageRelease(iref);

    return image;
}

static NSImage *decodeImageWithCocoa(NSData *inputData) {
    return [[NSImage alloc] initWithData:inputData];
}

static NSImage *readStdinImage(void) {
    NSFileHandle *stdinHandle = NULL;
    NSData *inputData = NULL;
    NSError *readError = NULL;

    if (!(stdinHandle = [NSFileHandle fileHandleWithStandardInput])) {
        return NULL;
    }

    if (!(inputData =
              [stdinHandle readDataToEndOfFileAndReturnError:&readError])) {
        LOG("Error reading file: %@", [readError localizedDescription]);
        return NULL;
    }

    if (isFarbfeldImage(inputData)) {
        return decodeFarbfeldImage(inputData);
    }

    if (isDecodableByCocoa(inputData)) {
        return decodeImageWithCocoa(inputData);
    }

    return NULL;
}

@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate> {
    NSWindow *window;
}
- (BOOL)createNewWindowWithImage:(NSImage *)imageData;
@end

@interface AppSubView : NSView {
    NSPoint dragPoint;
    id opaqueAppView;
}
@end

@interface AppView : NSImageView {
    AppSubView *subView;
    NSImage *image;
    NSString *dir, *file;
    NSMutableArray *files;
    NSInteger fileIdx;
    BOOL fromStdin;
    BOOL fileError;
    NSTimer *errorUpdate, *slideshowUpdate;
    CGSize usableScreen;
}
- (void)loadImageRestart;
- (void)magnifyFrameBy:(float)zoom;
@end

@implementation AppSubView
- (id)initWithFrame:(NSRect)frame andAppView:(AppView *)appView {
    if ((self = [super initWithFrame:frame])) {
        opaqueAppView = appView;
    }
    return self;
}

- (BOOL)acceptsFirstResponder {
    return YES;
}

- (void)keyDown:(NSEvent *)event {
    (void)event;
}

- (void)keyUp:(NSEvent *)event {
    switch ([event keyCode]) {
    case KEY_ESCAPE:
    case KEY_Q:
        [[self window] close];
        break;
    case KEY_MINUS:
        [(AppView *)opaqueAppView magnifyFrameBy:0.8f];
        break;
    case KEY_EQUALS:
        [(AppView *)opaqueAppView magnifyFrameBy:1.2f];
        break;
    default:
        LOG("Unrecognized key: 0x%x", [event keyCode]);
        break;
    }
}

- (void)mouseDown:(NSEvent *)theEvent {
    NSRect windowFrame = [[self window] frame];
    dragPoint = [NSEvent mouseLocation];
    dragPoint.x -= windowFrame.origin.x;
    dragPoint.y -= windowFrame.origin.y;
}

- (void)mouseDragged:(NSEvent *)theEvent {
    NSRect screenFrame = [[NSScreen mainScreen] frame];
    NSRect windowFrame = [self frame];
    NSPoint currentPoint = [NSEvent mouseLocation];
    NSPoint newOrigin =
        NSMakePoint(currentPoint.x - dragPoint.x, currentPoint.y - dragPoint.y);
    if ((newOrigin.y + windowFrame.size.height) >
        (screenFrame.origin.y + screenFrame.size.height)) {
        newOrigin.y = screenFrame.origin.y +
                      (screenFrame.size.height - windowFrame.size.height);
    }
    [[self window] setFrameOrigin:newOrigin];
}
@end

@implementation AppView
- (id)initWithFrame:(NSRect)frame andImage:(NSImage *)imageData {
    if (self = [super initWithFrame:frame]) {
        image = imageData;
        subView = [[AppSubView alloc] initWithFrame:frame andAppView:self];

        const NSRect screenSizeRect = [[NSScreen mainScreen] frame];

        const CGFloat screenWidth = screenSizeRect.size.width;
        const CGFloat screenHeight = screenSizeRect.size.height;

        const CGFloat menuBarHeight =
            [NSMenu menuBarVisible]
                ? [[[NSApplication sharedApplication] mainMenu] menuBarHeight]
                : 0.0f;

        usableScreen = CGSizeMake(screenWidth, screenHeight - menuBarHeight);

        [self addSubview:subView];
        [self loadImageRestart];
        [self setAnimates:enableResizeAnimations];
        [self setCanDrawSubviewsIntoLayer:YES];
        [self setImageScaling:NSImageScaleProportionallyUpOrDown];
    }
    return self;
}

- (void)loadImageRestart {
    [self setImage:image];
    [self forceResize:CGSizeMake([image size].width, [image size].height)
        enableAnimations:enableResizeAnimations];
}

- (NSSize)currentImageSize {
    return [image size];
}

- (void)forceResize:(CGSize)size enableAnimations:(BOOL)enabledAnims {
    NSRect frame = [[self window] frame];
    CGPoint center = CGPointMake(frame.origin.x + (frame.size.width / 2),
                                 frame.origin.y + (frame.size.height / 2));

    frame.size = CGSizeMake(floatMin(usableScreen.width, size.width),
                            floatMin(usableScreen.height, size.height));
    frame.origin = CGPointMake(center.x - (frame.size.width / 2),
                               center.y - (frame.size.height / 2));

    [[self window] setFrame:frame display:YES animate:enabledAnims];
    [subView
        setFrame:NSMakeRect(0.f, 0.f, frame.size.width, frame.size.height)];
    [self setNeedsDisplay:YES];
}

- (void)magnifyFrameBy:(float)zoom {
    NSSize newSize = NSMakeSize([self frame].size.width * zoom,
                                [self frame].size.height * zoom);
    [self forceResize:newSize enableAnimations:NO];
}

- (void)magnifyWithEvent:(NSEvent *)event {
    [self magnifyFrameBy:[event magnification] + 1.f];
}
@end

@implementation AppDelegate : NSObject
- (BOOL)createNewWindowWithImage:(NSImage *)imageData {
    CGSize screen = [[NSScreen mainScreen] frame].size;
    window = [[NSWindow alloc]
        initWithContentRect:NSMakeRect(screen.width / 2, screen.height / 2, 0.f,
                                       0.f)
                  styleMask:NSWindowStyleMaskResizable |
                            NSWindowStyleMaskTitled |
                            NSWindowStyleMaskFullSizeContentView
                    backing:NSBackingStoreBuffered
                      defer:NO];
    [window setTitle:@""];
    [window makeKeyAndOrderFront:nil];
    [window setMovableByWindowBackground:YES];
    [window setTitlebarAppearsTransparent:YES];
    [[window standardWindowButton:NSWindowZoomButton] setHidden:YES];
    [[window standardWindowButton:NSWindowCloseButton] setHidden:YES];
    [[window standardWindowButton:NSWindowMiniaturizeButton] setHidden:YES];
    [window setReleasedWhenClosed:NO];
    [[NSNotificationCenter defaultCenter]
        addObserver:self
           selector:@selector(windowWillClose:)
               name:NSWindowWillCloseNotification
             object:window];

    id view = [AppView alloc];
    if (![view initWithFrame:NSZeroRect andImage:imageData]) {
        WARN("Failed to load image");
        return NO;
    }
    [window setContentView:view];
    [view forceResize:[view currentImageSize]
        enableAnimations:enableResizeAnimations];

    return YES;
}

- (id)initWithImage:(NSImage *)imageData {
    if (self = [super init]) {
        id menubar = [NSMenu alloc];
        id appMenuItem = [NSMenuItem alloc];
        [menubar addItem:appMenuItem];
        [NSApp setMainMenu:menubar];
        id appMenu = [NSMenu alloc];
        id quitTitle = [@"Quit "
            stringByAppendingString:[[NSProcessInfo processInfo] processName]];
        id quitMenuItem =
            [[NSMenuItem alloc] initWithTitle:quitTitle
                                       action:@selector(terminate:)
                                keyEquivalent:@"q"];
        [appMenu addItem:quitMenuItem];
        [appMenuItem setSubmenu:appMenu];

        [self createNewWindowWithImage:imageData];

        if (!window) {
            PANIC("Failed to create application window");
        }
    }
    return self;
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)app {
    (void)app;
    return YES;
}

- (void)windowWillClose:(NSNotification *)notification {
    if ([notification object] == window) {
        window = nil;
    }
}
@end

int main(int argc, char *argv[]) {
    @autoreleasepool {
        [NSApplication sharedApplication];
        [NSApp setActivationPolicy:NSApplicationActivationPolicyAccessory];
        NSImage *stdinImage = readStdinImage();
        if (!stdinImage) {
            return EXIT_FAILURE;
        }
        AppDelegate *appDel = [[AppDelegate new] initWithImage:stdinImage];
        if (!appDel) {
            return EXIT_FAILURE;
        }
        [NSApp setDelegate:appDel];
        [NSApp activateIgnoringOtherApps:YES];
        [NSApp run];
    }
    return EXIT_SUCCESS;
}
