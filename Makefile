bleh: bleh.m
	 clang -mtune=native -O2 bleh.m -framework Cocoa -o bleh

app: bleh
	sh appify.sh -s bleh -n bleh

clean:
	rm -f bleh

fmt:
	@clang-format --style "{IndentWidth: 4}"

install: app
	mv -f bleh.app /Applications

default: app
all: default

.PHONY: app clean install default all fmt
